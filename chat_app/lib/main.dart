import 'package:chat_app/screens/auth.dart';
import 'package:chat_app/screens/chat.dart';
import 'package:chat_app/screens/splash.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'FlutterChat',
      theme: ThemeData().copyWith(
        colorScheme: ColorScheme.fromSeed(
          seedColor: const Color.fromARGB(255, 63, 17, 177),
        ),
      ),
      // The StreamBuilder widget is used to listen to changes in the authentication state.
// It listens to the authStateChanges stream provided by the FirebaseAuth instance.
// When the authentication state changes, the builder function is called with the updated snapshot.

      home: StreamBuilder(
        // Listen to the authStateChanges stream to get updates on authentication state.
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const SplashScreen();
          }
          // Check if there is data available in the snapshot, indicating that the user is authenticated.
          if (snapshot.hasData) {
            // If user is authenticated, return the ChatScreen widget.
            return const ChatScreen();
          } else {
            // If no data is available in the snapshot, indicating that the user is not authenticated,
            // return the AuthScreen widget to allow the user to login or signup.
            return const AuthScreen();
          }
        },
      ),
    );
  }
}
