import 'package:uuid/uuid.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

final formatter = DateFormat.yMd(); // for formatting dates

// created a class to add properties like title ,amount ,date etc
const uuid = Uuid(); // created object of uuid class its a utility object

enum Category { food, travel, leisure, work } //it allows to create custom type

const categoryIcons = {
  // for adding icons to all the categories
  Category.food: Icons.lunch_dining,
  Category.travel: Icons.flight_takeoff,
  Category.leisure: Icons.movie,
  Category.work: Icons.work,
};

class Expense {
  Expense({
    required this.title,
    required this.amount,
    required this.date,
    required this.category,
  }) : id = uuid
            .v4(); //uuid.v4() generates the unique string id(it is a initializer list to access property of uuid class)
  final String id;
  final String title;
  final double amount;
  final DateTime date;
  final Category category; //used to add categories of expenses

  get formattedDate {
    return formatter.format(date);
  }
}

// below clsss is to get the sum of total expenses
class ExpenseBucket {
  const ExpenseBucket({required this.category, required this.expenses});

  ExpenseBucket.forCategory(List<Expense>allExpenses,this.category):expenses=allExpenses.where((expense) => expense.category==category).toList();//additional construction function of class
  final Category category;
  final List<Expense> expenses;

  double get totalExpenses {
    double sum = 0;
    for (final expense in expenses) {
      sum += expense.amount;
    }
    return sum;
  }
}
