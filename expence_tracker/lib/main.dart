import 'package:flutter/material.dart';
import 'package:expence_tracker/widgets/expenses.dart';
//import 'package:flutter/services.dart';// this package is used to lock the rotaion of app

var kColorScheme = ColorScheme.fromSeed(
  seedColor: const Color.fromARGB(255, 96, 59, 181),
); //variable for setting ColorSchme

var kDarkColorScheme = ColorScheme.fromSeed(
  brightness: Brightness.dark,
  seedColor: const Color.fromARGB(255, 5, 99, 125),
); // this vairable used for adding dard mode to the app
void main() {
  //below commented code is to lock the device rotaion
  /*WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]).then((fn) {
  */
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      //below darkTheme is used to change the theme of app in dark mode
      darkTheme: ThemeData.dark().copyWith(
        useMaterial3: true,
        colorScheme: kDarkColorScheme,
        cardTheme: const CardTheme().copyWith(
          color: kDarkColorScheme.secondaryContainer,
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        ),
        // this below elevatedButtonTheme is used to theme that save expense button when user enterd expense data(IN DARK MODE)
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            backgroundColor: kDarkColorScheme.primaryContainer,
            foregroundColor: kDarkColorScheme.onPrimaryContainer,
          ),
        ),
      ),
      //below code is for theming app by using default parameters of flutter ,below themes are overrided using kColorScheme variable.
      // AND BELOW THEME IS LIGHT THEME
      theme: ThemeData().copyWith(
          useMaterial3: true,
          colorScheme: kColorScheme,
          appBarTheme: const AppBarTheme().copyWith(
            backgroundColor: kColorScheme.onPrimaryContainer,
            foregroundColor: kColorScheme.primaryContainer,
          ),
          // this below cardTheme is used to theme that expense cards which shown of start of screen that are added by user
          cardTheme: const CardTheme().copyWith(
            color: kColorScheme.secondaryContainer,
            margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          ),
          // this below elevatedButtonTheme is used to theme that save expense button when user enterd expense data(IN LIGHT MODE)
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              backgroundColor: kColorScheme.primaryContainer,
            ),
          ),
          //below code is to theme the texts in app
          textTheme: ThemeData().textTheme.copyWith(
                //below titleLarge is changes the theme of titles in app ex-expenses tracker text
                titleLarge: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: kColorScheme.onSecondaryContainer,
                  fontSize: 16,
                ),
              )),
      //scaffoldBackgroundColor: const Color.fromARGB(255, 220, 189, 252)),// it changes the background color of start screen
      //theme: ThemeData(useMaterial3: true),//convert app bar to white
      //......................................

      // themeMode: ThemeMode.system, // this is default mode by flutter
      home: const Expenses(), // add my expense class in material app
    ),
  );
  // });//lock device rotation
}
