import 'package:expence_tracker/widgets/chart/chart.dart';
import 'package:expence_tracker/widgets/expenses_list/expenses_list.dart';
import 'package:expence_tracker/models/expense.dart';
import 'package:expence_tracker/widgets/new_expense.dart';
import 'package:flutter/material.dart';

class Expenses extends StatefulWidget {
  //created a class named Expenses for UI for tool bar or app bar to insert new expense and chart and list of expenses, every time we add data and update ui so i used stateful widget
  const Expenses({super.key});
  @override
  State<StatefulWidget> createState() {
    return _ExpensesState();
  }
}

class _ExpensesState extends State<Expenses> {
  //it is connected to expenses class

  final List<Expense> _registeredExpenses = [
    //below code add the data in expense category
    Expense(
      title: 'Flutter Course',
      amount: 19.99,
      date: DateTime.now(),
      category: Category.work,
    ),
    Expense(
      title: 'Cinema',
      amount: 15.69,
      date: DateTime.now(),
      category: Category.leisure,
    ),
  ];

  void _openAddExpenseOverlay() {
    // for clicking on + icon on app so it can go to that page
    showModalBottomSheet(
        isScrollControlled: true, //it sets full screen to add expense page
        useSafeArea: true,
        context: context,
        builder: (ctx) => NewExpense(onAddExpense: _addExpense));
  }

  void _addExpense(Expense expense) {
    //this mehtod is for adding expenses to show on screen
    setState(() {
      _registeredExpenses.add(expense);
    });
  }

  void _removeExpense(Expense expense) {
    final expenseIndex =
        _registeredExpenses.indexOf(expense); //to get index of index
    // for removing expenses by sliding
    setState(() {
      _registeredExpenses.remove(expense);
    });

    ScaffoldMessenger.of(context).clearSnackBars();
    //below code is to undo the deleted expense in 3 seconds.
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: const Duration(seconds: 3),
      //backgroundColor: Colors.amber,
      content: const Text('Expense Deleted'),
      action: SnackBarAction(
          label: 'Undo',
          onPressed: () {
            setState(() {
              //setState is for update the UI when Undo clicked
              _registeredExpenses.insert(expenseIndex, expense);
            });
          }),
    ));
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    Widget mainContent = const Center(
      // this widget is to show the message when no expense is font
      child: Text('No expenses found. Start adding some'),
    );
    if (_registeredExpenses.isNotEmpty) {
      mainContent = ExpensesList(
        expenses: _registeredExpenses,
        onRemoveExpense: _removeExpense,
      );
    }
    return Scaffold(
      // used to add gerneral UI
      appBar: AppBar(
        title: const Text('EXPENSES TRACKER'),
        actions: [
          IconButton(
            onPressed: _openAddExpenseOverlay,
            icon: const Icon(
                Icons.add), // to add the + button on right side of app bar
          ),
        ],
      ),

      body: width < 600
          ? Column(
              //column used to add chart above list of expenses, and two widget above each other
              children: [
                // childs of column
                //const Text('The Chart'),
                Chart(expenses: _registeredExpenses),
                Expanded(
                  child: mainContent, // added the expenses list widget
                ),
              ],
              //below code is for switchin colum to row depending how much width is avalilable when rotated device.
            )
          : Row(children: [
              // we expanded the chart widget to adjust the row width when it rotated
              Expanded(child: Chart(expenses: _registeredExpenses)),
              Expanded(
                child: mainContent,
              )
            ]),

      /*
      body: Text('The chart'),
      
      backgroundColor: Colors.amber,// changes the color of whole screen and text color becomes black by default 
      */
    );
  }
}
