import 'package:flutter/material.dart';

// This widget represents a bar in a chart. It takes a 'fill' parameter to determine its height.
class ChartBar extends StatelessWidget {
  // Constructor for the ChartBar widget.
  const ChartBar({
    Key? key,
    required this.fill,
  }) : super(key: key);

  // The fill parameter represents the height of the bar in the chart.
  final double fill;

  // Build method is required for every StatelessWidget. It returns the visual representation of the widget.
  @override
  Widget build(BuildContext context) {
    // Determine if the device is in dark mode.
    final isDarkMode =
        MediaQuery.of(context).platformBrightness == Brightness.dark;

    // Expanded widget ensures that the ChartBar takes all available vertical space in its parent.
    return Expanded(
      // Padding adds spacing around the ChartBar.
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4),
        // FractionallySizedBox scales its child (DecoratedBox) based on the fraction of the available height.
        child: FractionallySizedBox(
          // The heightFactor is set to the 'fill' parameter, determining the height of the bar in the chart.
          heightFactor: fill,
          // DecoratedBox allows custom decoration for the box.
          child: DecoratedBox(
            // BoxDecoration provides various styling options like color, shape, and border radius.
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              // Adding a rounded border to the top of the box.
              borderRadius:
                  const BorderRadius.vertical(top: Radius.circular(8)),
              // The color of the box is determined based on the device's theme.
              color: isDarkMode
                  ? Theme.of(context).colorScheme.secondary
                  : Theme.of(context).colorScheme.primary.withOpacity(0.65),
            ),
          ),
        ),
      ),
    );
  }
}
