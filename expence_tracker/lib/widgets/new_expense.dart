import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:expence_tracker/models/expense.dart';

final formatter = DateFormat.yMd();

// this class is for adding new expenses to the app
class NewExpense extends StatefulWidget {
  const NewExpense({super.key, required this.onAddExpense});
  final void Function(Expense expense) onAddExpense;
  @override
  State<NewExpense> createState() {
    return _NewExpenseState();
  }
}

class _NewExpenseState extends State<NewExpense> {
  final _titleController = TextEditingController();
  final _amountController = TextEditingController();
  DateTime? _selectedDate;
  Category _selectedCategory =
      Category.leisure; // to by default select the category

  void _presentDatePicker() async {
    // this function is used to pick the date for that datetime widget after amount button
    final now = DateTime.now();
    final firstDate = DateTime(now.year - 1, now.month, now.day);
    //  2nd way - when I use await below then I can get access to store that showdatepicker property in a variable
    final pickedDate = await showDatePicker(
      context: context,
      initialDate: now,
      firstDate: firstDate,
      lastDate: now,
    );
    setState(() {
      // it stores the date picked by the user
      _selectedDate = pickedDate;
    });
  }

  // below method created to show for which platform app should run android or ios using if else condition
  void _showDialog() {
    if (Platform.isIOS) {
      showCupertinoDialog(
        context: context,
        builder: (ctx) => CupertinoAlertDialog(
          title: const Text('Invalid Input'),
          content: const Text(
            'Please make sure a valid title, amount, date, and category were entered...',
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(ctx);
              },
              child: const Text('Okay'),
            ),
          ],
        ),
      );
    } else {
      // below dialog box used to show the pop-up box for showing error as text("invalid input") and below that text, it shows the content for the user what the error is and the 'Okay' (FOR ANDROID DEVICE)
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: const Text('Invalid Input'),
          content: const Text(
            'Please make sure a valid title, amount, date, and category were entered...',
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(ctx);
              },
              child: const Text('Okay'),
            ),
          ],
        ),
      );
    }
  }

  void _submitExpenseData() {
    // this method is for submitting the expense data when the user selects the category and inserts data
    final enteredAmount = double.tryParse(_amountController
        .text); // used to correct the invalid number given by the user, so it tryparse('hello')=null and tryParse('1.12')=1.12
    // below variable is for if the amount is null or is less than 0, that is invalid
    final amountIsInvalid = enteredAmount == null || enteredAmount <= 0;
    // below if condition checks the title after trimming with its extra spaces and if it is empty or not and used or (||) operator with amountIsInvalid and with _selectedDate if no date has been selected
    if (_titleController.text.trim().isEmpty ||
        amountIsInvalid ||
        _selectedDate == null) {
      // below dialog box used to show the pop-up box for showing error as text("invalid input") and below that text, it shows the content for the user what the error is and the 'Okay' (FOR ANDROID DEVICE)
      _showDialog();
      return;
    }
    // below code is to pass the all the values to addExpense function
    widget.onAddExpense(
      Expense(
        title: _titleController.text,
        amount: enteredAmount,
        date: _selectedDate!,
        category: _selectedCategory,
      ),
    );

    Navigator.pop(context);
  }

  @override
  void dispose() {
    // to delete the controller when the widget is not in use anymore
    _titleController.dispose();
    _amountController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final keyboardSpace = MediaQuery.of(context).viewInsets.bottom;
    return LayoutBuilder(builder: (ctx, constraints) {
      final width = constraints.maxWidth;
      return SizedBox(
        // wrapped single child scrollable with side box so it can take as much as it can take
        height: double.infinity,
        child: SingleChildScrollView(
          // we wrapped this widget to single child scrollable for scrolling the add expense page when clicked on + icon by using this when we add expense title the keyboard's open and that page now scrollable so it can't overlap on it
          child: Padding(
            padding: EdgeInsets.fromLTRB(20, 16, 16, keyboardSpace + 16),
            child: Column(
              children: [
                if (width >= 600 ||
                    MediaQuery.of(context).orientation == Orientation.landscape)
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: TextField(
                          controller: _titleController,
                          // below commented code is for way 1
                          // onChanged: _saveTitleInput,
                          maxLength: 50,
                          decoration: const InputDecoration(
                            label: Text('Title'),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        child: TextField(
                          controller: _amountController,
                          keyboardType: TextInputType.number,
                          decoration: const InputDecoration(
                            prefixText: '\₹',
                            label: Text('Amount'),
                          ),
                        ),
                      ),
                    ],
                  )
                else
                  TextField(
                    controller: _titleController,
                    // below commented code is for way 1
                    // onChanged: _saveTitleInput,
                    maxLength: 50,
                    decoration: const InputDecoration(
                      label: Text('Enter Title'),
                    ),
                  ),
                if (width >= 600 ||
                    MediaQuery.of(context).orientation == Orientation.landscape)
                  Row(
                    children: [
                      DropdownButton(
                        value: _selectedCategory,
                        // this value shows the selected category
                        items: Category.values
                            .map((category) => DropdownMenuItem(
                                  value: category,
                                  child: Text(category.name.toUpperCase()),
                                ))
                            .toList(),
                        onChanged: (value) {
                          setState(() {
                            if (value == null) {
                              return;
                            }
                            _selectedCategory = value;
                          });
                        },
                      ),
                      const SizedBox(
                        width: 24,
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment
                              .end, // it adds the date(calender) widget at the end of that row
                          crossAxisAlignment: CrossAxisAlignment
                              .center, // it placed that date button at the center
                          // this calendar date button is placed after the amount button
                          children: [
                            // in this below text widget if no date is picked then it shows that text but if we picked date then it shows the date
                            Text(_selectedDate == null
                                ? 'No Date Selected'
                                : formatter.format(_selectedDate!)),
                            IconButton(
                              onPressed: _presentDatePicker,
                              // we have passed that datepicker function in this onPressed
                              icon: const Icon(Icons.calendar_month),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                else
                  Row(
                    // this row is used to show the date picker after the amount button that's why in this widget TextField of amount button also added
                    children: [
                      Expanded(
                        child: TextField(
                          controller: _amountController,
                          keyboardType: TextInputType.number,
                          decoration: const InputDecoration(
                            prefixText: '₹',
                            label: Text('Enter Amount'),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment
                              .end, // it adds the date(calender) widget at the end of that row
                          crossAxisAlignment: CrossAxisAlignment
                              .center, // it placed that date button at the center
                          // this calendar date button is placed after the amount button
                          children: [
                            // in this below text widget if no date is picked then it shows that text but if we picked date then it shows the date
                            Text(_selectedDate == null
                                ? 'No Date Selected'
                                : formatter.format(_selectedDate!)),
                            IconButton(
                              onPressed: _presentDatePicker,
                              // we have passed that datepicker function in this onPressed
                              icon: const Icon(Icons.calendar_month),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                // this sized box adds the vertical space between the amount button and category dropdown
                const SizedBox(
                  height: 16,
                ),
                if (width >= 600 ||
                    MediaQuery.of(context).orientation == Orientation.landscape)
                  Row(
                    children: [
                      const Spacer(),
                      // it adds the horizontal space between the category dropdown button and cancel, save expense button
                      TextButton(
                        onPressed: () {
                          Navigator.pop(
                              context); // to close that add expense page
                        },
                        child: const Text('Cancel'),
                      ),
                      ElevatedButton(
                        onPressed: _submitExpenseData,
                        // this is to submit the expense given by the user, for submitting data we created the method name _submitExpenseData
                        child: const Text('Save Expense'),
                      ),
                    ],
                  )
                else
                  Row(
                    children: [
                      // this dropdown button is used to select the category of expense
                      // in below DropdownButon -item: needs a list of dropdown menu of category, Category.values displayes the list of all the categories.
                      // the category.name.toUpperCase() converts the category names to upper case by default these are in lower case
                      DropdownButton(
                        value: _selectedCategory,
                        // this value shows the selected category
                        items: Category.values
                            .map((category) => DropdownMenuItem(
                                  value: category,
                                  child: Text(category.name.toUpperCase()),
                                ))
                            .toList(),
                        onChanged: (value) {
                          setState(() {
                            if (value == null) {
                              return;
                            }
                            _selectedCategory = value;
                          });
                        },
                      ),
                      const Spacer(),
                      // it adds the horizontal space between the category dropdown button and cancel, save expense button
                      TextButton(
                        onPressed: () {
                          Navigator.pop(
                              context); // to close that add expense page
                        },
                        child: const Text('Cancel'),
                      ),
                      ElevatedButton(
                        onPressed: _submitExpenseData,
                        // this is to submit the expense given by the user, for submitting data we created the method name _submitExpenseData
                        child: const Text('Save Expense'),
                      ),
                      /*
                        // way 1 of getting input from the user
                        ElevatedButton(
                          onPressed: () {
                            print(_enteredTitle);
                          },
                          child: const Text("Save Expense"),
                        ),
                        */ // this button is to save the input given by the user
                    ],
                  )
              ],
            ),
          ),
        ),
      );
    });
  }
}
