import 'package:expence_tracker/models/expense.dart';
import 'package:flutter/material.dart';

// class used to make new widget for show the expenses items
class ExpenseItem extends StatelessWidget {
  const ExpenseItem(this.expense, {super.key});
  final Expense expense;
  @override
  Widget build(BuildContext context) {
    /*
    return Card(
      child: Text(expense.title),
    ); //used to styling show card looks
    */
    return Card(
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 16,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment
              .start, // it aligns the expense title to the start of screen(flutter course,cinema)
          children: [
            Text(
              expense.title,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(height: 4),
            Row(
              children: [
                Text(
                    '\₹${expense.amount.toStringAsFixed(2)}'), //amount.toStringAsFixed(2) in this the 2 is the numbers after decimal point ex 70.56 and that \₹ symbol is used to show to dolloer symbol like $10
                const Spacer(), // used to add between row and column and it take all the remaining space
                Row(
                  children: [
                    Icon(categoryIcons[expense.category]),
                    const SizedBox(
                      width: 8,
                    ),
                    Text(expense.formattedDate),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
