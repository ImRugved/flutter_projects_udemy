import 'package:expence_tracker/models/expense.dart';
import 'package:expence_tracker/widgets/expenses_list/expense_item.dart';
import 'package:flutter/material.dart';
//class is used to creating widget for show the list of expenses

class ExpensesList extends StatelessWidget {
  const ExpensesList({
    super.key,
    required this.expenses,
    required this.onRemoveExpense,
  });
  final List<Expense> expenses; //list of expenses
  final void Function(Expense expense) onRemoveExpense;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: expenses.length,
      //by using dismissible widget we can pop the expense with sliding
      itemBuilder: (ctx, index) => Dismissible(
          key: ValueKey(expenses[index]),
          //below background container added and in that color property we added a color scheme which show a background when we slide expense to delete
          background: Container(
            color: Theme.of(context).colorScheme.error.withOpacity(0.75),
            //above withOpacity gives the opacity to that background color it faints that color
            margin: EdgeInsets.symmetric(
              horizontal: Theme.of(context).cardTheme.margin!.horizontal,
            ),
          ),
          onDismissed: (direction) {
            onRemoveExpense(expenses[index]);
          },
          child: ExpenseItem(expenses[index])),
      //itemCount used to difines how many list items render with expense.length,if item count is 2 then that itembuilder call 2 times , we give th expense list to Text widget by using buidler
    );
  }
}
