import 'package:flutter/material.dart';
import 'package:meals_app/data/dummy_data.dart';
import 'package:meals_app/models/meal.dart';
import 'package:meals_app/screens/meals.dart';
import 'package:meals_app/widgets/category_grid_item.dart';
import 'package:meals_app/models/category.dart';

// below class is to add  a categories of food screeen on app(MAIN SCREEN)
//conveted it in statefulwidget to add animation to main page
class CategoriesScreen extends StatefulWidget {
  const CategoriesScreen(
      {super.key,
      //required this.onToggleFovorites,
      required this.availableMeals});
  //final void Function(Meal meal) onToggleFovorites;
  final List<Meal> availableMeals;

  @override
  State<CategoriesScreen> createState() => _CategoriesScreenState();
}

//below class connnect with mixin class named SingleTickerProviderStateMixin using with keyword used to use various feature of animation class
class _CategoriesScreenState extends State<CategoriesScreen>
    with SingleTickerProviderStateMixin {
  //below variable and initstate method used to set animations to main page
  late AnimationController _animationController; //variable has value as as used
  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
      lowerBound: 0,
      upperBound: 1,
    );
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

//below method used to add new page when tapped on meal category
  void _selectCategory(BuildContext context, Category category) {
    final filteredMeals = widget.availableMeals
        .where((meal) => meal.categories.contains(category.id))
        .toList(); //it return the meal of that category id ha
    Navigator.of(context).push(MaterialPageRoute(
        // it shows the meal categories when tapped on any meal menu on main screen
        builder: (ctx) => MealsScreen(
              title: category.title,
              meals: filteredMeals,
              //onToggleFavorites: onToggleFovorites,
            ))); //Navigator.push(context, route)
  }

  @override
  Widget build(BuildContext context) {
    return /*Scaffold(
      appBar: AppBar(
        title: const Text('Pick Your Category'),
      ),
      body:*/
        //we have removed the Sccaffold because in tabs file we added it it show two title bar thats why we removed it from here
        //wrapped gridview with animatedBuider to add the animations
        AnimatedBuilder(
            animation: _animationController,
            child: GridView(
              padding: const EdgeInsets.all(
                  20), //it add the space from all the sides of app on the categories widgets
              //below grid deglates add the spacing ,adjustmet for all the category widgets which shows the title of meals with multiple colors
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 3 / 2,
                crossAxisSpacing: 15,
                mainAxisSpacing: 20,
              ),
              children: [
                /*
          Text(
            '1',
            style: TextStyle(color: Colors.white),
          ),
          Text(
            '2',
            style: TextStyle(color: Colors.white),
          ),
          Text(
            '3',
            style: TextStyle(color: Colors.white),
          ),
          Text(
            '4',
            style: TextStyle(color: Colors.white),
          ),
          Text(
            '5',
            style: TextStyle(color: Colors.white),
          ),
          Text(
            '6',
            style: TextStyle(color: Colors.white),
          ),
          */
                //availableCategoires.map((category)=>CategoryGridItem(category:category) for this we have alternative code below
                // this for loop shows the all the dummy categories in screen
                for (final category in availableCategories)
                  CategoryGridItem(
                    category: category,
                    onSelectCategory: () {
                      _selectCategory(context, category);
                    },
                  )
              ],
            ),
            builder: (context, child) =>
                //2nd way using slidetransition
                SlideTransition(
                  position: Tween(
                    begin: const Offset(0, 0.3),
                    end: const Offset(0, 0),
                  ).animate(CurvedAnimation(
                      parent: _animationController, curve: Curves.easeInOut)),
                  child: child,
                )
            /*
            1st way to add animation 
            Padding(
                  padding: EdgeInsets.only(
                    top: 100 - _animationController.value * 100,
                  ),
                  child: child,
                )*/
            );
    //),
    //);
  }
}
