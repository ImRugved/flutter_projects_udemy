import 'package:flutter/material.dart';
//import 'package:meals_app/data/dummy_data.dart';
//import 'package:meals_app/models/meal.dart';
import 'package:meals_app/provider/favorites_provider.dart';
import 'package:meals_app/screens/categories.dart';
import 'package:meals_app/screens/filters.dart';
import 'package:meals_app/screens/meals.dart';
import 'package:meals_app/widgets/main_drawer.dart';
//import 'package:meals_app/provider/meals_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
//import 'package:meals_app/provider/meals_provider.dart';
import 'package:meals_app/provider/filters_provider.dart';

const kInitialFilters = {
  Filter.glutenFree: false,
  Filter.vegetarian: false,
  Filter.vegan: false,
  Filter.lactoseFree: false,
};

// this class is used to add the tab bar below the app to switch to fav meal option and many more
// added riverpod package so we have to replace statefulwidget to ConsumerStatefulWidget
class TabsScreen extends ConsumerStatefulWidget {
  const TabsScreen({super.key});

  @override
  ConsumerState<TabsScreen> createState() {
    return _TabsScreenState();
  }
}

class _TabsScreenState extends ConsumerState<TabsScreen> {
  int _selectedPageIndex =
      0; // to get the index of two pages to switch them using tab bar
  //final List<Meal> _favoriteMeals = []; //it add the favorite meal to list
  //Map<Filter, bool> _selectedFilters = kInitialFilters;
/*
// below method used to show the snackbar message to know the user that meal added or removed from fav tab
  void _showInfoMessage(String message) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }
  */
/*
  //below funciton add meal or remove from favorite
  void _toggleMealFavoriteStatus(Meal meal) {
    //below isExisting varible check the that fav meals contains this meal in this function
    final isExisting = _favoriteMeals.contains(meal);
    // if condition check the fav is exits then it will remove if not then it added the meal to favorite
    if (isExisting) {
      setState(() {
        _favoriteMeals.remove(meal);
      });
      _showInfoMessage('Meal is no longer favorite');
    } else {
      setState(() {
        _favoriteMeals.add(meal);
        _showInfoMessage('Marked as a favorite!');
      });
    }
  }
  */

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  void _setScreen(String identifier) async {
    Navigator.of(context).pop();
    if (identifier == 'filters') {
      await Navigator.of(context).push<Map<Filter, bool>>(MaterialPageRoute(
        builder: (ctx) => const FiltersScreen(),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    final availableMeals = ref.watch(filteredMealsProvider);
    Widget activePage = CategoriesScreen(
      //onToggleFovorites: _toggleMealFavoriteStatus,
      availableMeals: availableMeals,
    ); //it cantains one of these two pages and it gives to the body
    var activePageTitle = 'Categories';
    //if 2nd tab selected means favorites tab selected then switch the active screeen to MealsScreen
    if (_selectedPageIndex == 1) {
      final favoriteMeals = ref.watch(favoriteMealsProvider);
      activePage = MealsScreen(
        meals: favoriteMeals,
        //onToggleFavorites: _toggleMealFavoriteStatus,
      );
      activePageTitle = 'Your Favorites';
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(activePageTitle),
      ),
      drawer: MainDrawer(
        onSelectScreen: _setScreen,
      ), //added the MainDrawer class widget here
      body: activePage,
      //below bottomNavigationBar adds tap bar
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        currentIndex: _selectedPageIndex, //it control  which tap is highligted
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.set_meal), label: "Categories"),
          BottomNavigationBarItem(icon: Icon(Icons.star), label: "Favourites"),
        ],
      ),
    );
  }
}
