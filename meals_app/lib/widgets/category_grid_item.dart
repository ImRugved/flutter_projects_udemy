import 'package:meals_app/models/category.dart';
import 'package:flutter/material.dart';

// this class is used to add and use the widget in home screen to show the categories of meals
class CategoryGridItem extends StatelessWidget {
  const CategoryGridItem(
      {super.key, required this.category, required this.onSelectCategory});
  final Category category;
  // below onSelectdCategory function add the arrow buton to back on main screen when tapped on meal category
  final void Function() onSelectCategory;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      // wrapped the container with Inkwell widget for making all the categories widgets tappeble and adjust the styling of the widgets.
      onTap: onSelectCategory,
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(14),
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
                16), //makes the borders of all categories widgets circular
            gradient: LinearGradient(
              colors: [
                category.color.withOpacity(0.55),
                category.color.withOpacity(0.9),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomLeft,
            )),
        child: Center(
          child: Text(
            category.title,
            style: Theme.of(context).textTheme.titleLarge!.copyWith(
                  color: Theme.of(context).colorScheme.onBackground,
                ),
          ),
        ),
      ),
    );
  }
}
