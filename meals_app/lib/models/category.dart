import 'package:flutter/material.dart';
// class is used to add the categories data of meals
class Category {
  const Category(
      {required this.id, required this.title, this.color = Colors.orange});
  final String id;
  final String title;
  final Color color;
}
