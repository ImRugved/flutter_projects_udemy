import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StartScreen extends StatelessWidget {
  StartScreen(this.startQuiz, {Key? key});

  final void Function() startQuiz;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // Uncomment the lines below if you want to use Opacity
          // Opacity(
          //   opacity: 0.6,
          //   child: Image.asset(
          //     'assets/images/quiz-logo.png',
          //     width: 300,
          //   ),
          // ),

          // Use ColorFiltered for a color overlay
          const SizedBox(
            height: 180,
          ),
          ColorFiltered(
            colorFilter: const ColorFilter.mode(
              Color.fromARGB(146, 255, 255, 255),
              BlendMode.dst,
            ),
            child: Image.asset(
              'assets/images/quiz-logo.png',
              width: 300,
            ),
          ),

          const SizedBox(height: 40),
          Center(
            child: Text(
              'Learn Flutter the fun way!',
              style: GoogleFonts.lato(
                color: const Color.fromARGB(255, 123, 0, 255),
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 30),
          OutlinedButton.icon(
            onPressed: startQuiz,
            style: OutlinedButton.styleFrom(
              foregroundColor: const Color.fromARGB(255, 3, 3, 3),
            ),
            icon: const Icon(
              Icons.arrow_right_alt,
              size: 30,
            ),
            label: const Text(
              'Start Quiz',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),

          const Expanded(
            child: Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  '@Developed by Rugved Belkundkar',
                  style: TextStyle(
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
