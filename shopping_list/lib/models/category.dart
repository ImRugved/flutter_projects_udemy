import 'package:flutter/material.dart';
// in this file we have added a enum data type to add a names of categories for shopping 
enum Categories {
  vegetables,
  fruit,
  meat,
  dairy,
  carbs,
  sweets,
  spices,
  convenience,
  hygiene,
  other
}
//in this below class we have create two variables as title and color which is used to set the title and color to the categories
class Category {
  const Category(this.title,this.color);
  final String title;
  final Color color;
}
