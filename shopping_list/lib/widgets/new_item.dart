import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopping_list/data/categories.dart';
//import 'package:shopping_list/data/dummy_items.dart';
import 'package:shopping_list/models/category.dart';
//import 'package:shopping_list/models/grocery_item.dart';
import 'package:http/http.dart' as http;
import 'package:shopping_list/models/grocery_item.dart'; // created object as http
// this NewItem class widgets created new screen which is used to add the new grocery items in the list of Groceries and it extends statefulwidget because it is dynamic class user can add the input thats why.

// this NewItem class widget is passed to icon button from home screen (GroceryList class) when user tap the button on app bar it navigate that screen to this NewItem screen
class NewItem extends StatefulWidget {
  const NewItem({super.key});
  @override
  State<NewItem> createState() {
    return _NewItemState();
  }
}

class _NewItemState extends State<NewItem> {
  // _formKey -
  final _formKey = GlobalKey<FormState>();
  var _enteredName = '';
  var _enteredQuantity = 1;
  var _selectedCategory = categories[
      Categories.vegetables]!; // to set the default category as vegetables
  var _isSending =
      false; //used to show the loading screen when adding new items
  //below method is addd to save the entered data by user and it also validate the user input by .validate() method
  void _saveItem() async {
    if (_formKey.currentState!.validate()) {
      setState(() {
        _isSending = true;
      });
      _formKey.currentState!
          .save(); // to sav and store the entered data and press button

      /*
      print(_enteredName); //prints the entered name
      print(_enteredQuantity);//prints the entered quantity
      print(_selectedCategory);//prints the selected category
      */
      //BELOW CODE IS TO CONNECT FIREBASE BACKEND WITH PROJECT TO PUSH DATA IN BACKEND
      final url = Uri.https(
        'udemy-flutter-99aea-default-rtdb.firebaseio.com',
        '/shopping-list.json',
      );

      final response = await http.post(url,
          headers: {
            'Content-Type': 'application/json',
          },
          body: json.encode({
            //encode-to convert map to jason
            'name': _enteredName,
            'quantity': _enteredQuantity,
            'category': _selectedCategory.title,
          }));
      final Map<String, dynamic> resData = json.decode(response.body);
      print(response.body);
      print(response.statusCode);
      if (!context.mounted) {
        return;
      }
      Navigator.of(context).pop(GroceryItem(
          id: resData['name'],
          name: _enteredName,
          quantity: _enteredQuantity,
          category: _selectedCategory));

      // below navigator passes data given by user to grocery list
      /*
      Navigator.of(context).pop(GroceryItem(
          id: DateTime.now().toString(),
          name: _enteredName,
          quantity: _enteredQuantity,
          category: _selectedCategory));
    */
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 0, 57, 67),
      appBar: AppBar(
        foregroundColor: const Color.fromARGB(255, 0, 45, 67),
        backgroundColor: const Color.fromARGB(255, 255, 191, 0),
        title: const Text('Add a New Item',
            style:
                TextStyle(fontSize: 28, color: Color.fromARGB(255, 0, 66, 97))),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 28, 10, 18),

        // in the body of Scaffold of NewItem widget class we added the Form widget to child so it can take input from user and show on screen
        child: Form(
          key: _formKey,
          child: Column(
            // in this we have added a TextFormField widget so user can give the input
            children: [
              TextFormField(
                style: const TextStyle(fontSize: 20),
                maxLength:
                    50, // user can add the title of new grocery item of max lenght 50

                decoration: const InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(14),
                    ),
                  ),
                  label: Text('Name',
                      style: TextStyle(fontSize: 20, color: Colors.white)),
                ),
                //belwo validator function used to validate that buttons so it can validate the user data or show error message when data is null or empty and if the value is correct it returns the null
                validator: (value) {
                  if (value == null ||
                      value.isEmpty ||
                      value.trim().length <= 1 ||
                      value.trim().length > 50) {
                    return 'Must be between 1 and 50 character..';
                  }
                  // above if conditions fails means value given by used is correct then return null
                  return null;
                },
                // below onSaved is set the entered data to the value parameter and it saves the input from user
                onSaved: (value) {
                  _enteredName = value!;
                },
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(right: 16),
                      padding: const EdgeInsets.fromLTRB(0, 0, 26, 0),
                      child: TextFormField(
                        style: const TextStyle(fontSize: 20),
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(14))),
                          label: Text('Quantity',
                              style:
                                  TextStyle(fontSize: 20, color: Colors.white)),
                        ),
                        keyboardType: TextInputType.number,
                        initialValue: _enteredQuantity.toString(),
                        //set initial value to text inut field it works with string becoz every user input it as a string

                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              int.tryParse(value) == null ||
                              int.tryParse(value)! <= 0) {
                            return 'Must be a valid positive number';
                          }
                          // above if conditions fails means value given by used is correct then return null

                          return null;
                        },
                        onSaved: (value) {
                          _enteredQuantity = int.parse(
                              value!); //convets the string to int and save it
                        },
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 1,
                  ),
                  // after quantity TextFormField widget we added added a dropdownButton which show the all th ecategory types so user can select that category and the category item with name and quantity
                  Expanded(
                    child: Container(
                      //padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      //margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: DropdownButtonFormField(
                          dropdownColor: const Color.fromARGB(255, 0, 57, 67),

                          //below value set the defalut category
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(14))),
                          ),
                          value: _selectedCategory,
                          iconSize: 20,
                          style: const TextStyle(fontSize: 20),
                          items: [
                            for (final category in categories.entries)
                              DropdownMenuItem(
                                value: category.value,
                                child: Row(
                                  children: [
                                    Container(
                                      width: 16,
                                      height: 16,
                                      color: category.value.color,
                                    ),
                                    const SizedBox(
                                      width: 6,
                                    ),
                                    Text(
                                      category.value.title,
                                      style: const TextStyle(fontSize: 16),
                                      selectionColor: Colors.amber,
                                    ),
                                  ],
                                ),
                              ),
                          ],
                          onChanged: (value) {
                            setState(() {
                              _selectedCategory = value!;
                            });
                          }),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 12,
              ),
              // below row add the two buttons to resent entered data or to add entered data from user
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: _isSending
                          ? null
                          : () {
                              _formKey.currentState!.reset();
                            },
                      child: const Text(
                        'Reset',
                        style: TextStyle(color: Colors.amber, fontSize: 20),
                        selectionColor: Colors.amber,
                      )),
                  ElevatedButton(
                      onPressed: _isSending ? null : _saveItem,
                      child: _isSending
                          ? const SizedBox(
                              height: 16,
                              width: 16,
                              child: CircularProgressIndicator(),
                            )
                          : const Text('Add item',
                              style: TextStyle(
                                  color: Colors.amber,
                                  fontSize:
                                      20))), // passed the _saveItem function which used to save the entered data by user when this Add item button is pressed
                  // it triggers the _saveItem method
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
