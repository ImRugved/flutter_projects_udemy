import 'package:flutter/material.dart';
import 'package:shopping_list/data/categories.dart';
//import 'package:shopping_list/models/category.dart';
//import 'package:shopping_list/data/dummy_items.dart';
//WE CAN DELETE DUMMY_ITEMS.DART FILE
import 'package:shopping_list/models/grocery_item.dart';
import 'package:shopping_list/widgets/new_item.dart';
import 'package:http/http.dart' as http; // created object as http
import 'dart:convert';

// in this GroceryList class includes the list of groceries
// this is stateful widgt becauz we have use he item list in this class so user can add it
class GroceryList extends StatefulWidget {
  const GroceryList({Key? key}) : super(key: key);

  @override
  State<GroceryList> createState() => _GroceryListState();
}

class _GroceryListState extends State<GroceryList> {
  List<GroceryItem> _groceryItems = [];
  var _isLoading =
      true; // used for adding loading screen when data is fetching when reloded
  String? _error;

  @override
  //below initState is to init the objext of class which loads the data when _loadItems method is called
  void initState() {
    super.initState();
    _loadItems();
  }

//below method is to load the data form backend on main screen
  void _loadItems() async {
    final url = Uri.https(
      'udemy-flutter-99aea-default-rtdb.firebaseio.com',
      '/shopping-list.json',
    );
    try {
      final response = await http.get(url); // sent a get request to fethch data
      //joson.decode to convert josn to map to fetch data

      if (response.statusCode >= 400) {
        setState(() {
          _error = 'Failed to fetch data. Please try again later.';
        });
      }

      if (response.body == 'null') {
        setState(() {
          _isLoading = false;
        });
        return;
      }

      final Map<String, dynamic> listData = json.decode(response.body);
      final List<GroceryItem> loadedItems = [];

      for (final item in listData.entries) {
        final category = categories.entries
            .firstWhere(
              (catItem) => catItem.value.title == item.value['category'],
            )
            .value;

        loadedItems.add(
          GroceryItem(
            id: item.key,
            name: item.value['name'],
            quantity: item.value['quantity'],
            category: category,
          ),
        );
      }

      setState(() {
        _groceryItems = loadedItems;
        _isLoading = false; // assign false when i have my loaded items
      });
    } catch (error) {
      setState(() {
        _error = 'Something went wrong! Please try again later.';
      });
    }
  }

  //---------------------------------------------------------
/*
  // belwo addItem method is used it add the items in a grocery from user
  // This method, '_addItem', is responsible for navigating to a new screen in a Flutter application.
// It uses the 'Navigator' widget to push a new route onto the navigator's stack.

// The 'Navigator' widget manages a stack of screens or pages and is used for navigation.
// The 'of' method retrieves the nearest Navigator ancestor in the widget tree.
// It takes a 'BuildContext' object as an argument, typically denoted by 'context'.
//BY USING ASYNC AND AWAIT THE DATA ENTERED BY USER PASSES TO _GROCERYITEMS LIST FROM NEW ITEM CLASS WHICH IS IN NEW_ITEM.DART FILE IN SIDE THE _SaveItem function
  void _addItem() async {
    // The 'push' method is used to push a new route onto the navigator's stack.
    // In this case, it navigates to a new screen represented by the 'NewItem'(class) widget.

    // MaterialPageRoute is a built-in Flutter class that defines a material page route.
    // It represents a full-screen page that slides in from the right.
    final newItem = await Navigator.of(context).push<GroceryItem>(
      // The 'builder' property takes a callback function that returns the widget to be displayed on the new page.
      // In this case, it creates and returns an instance of the 'NewItem' widget.
      MaterialPageRoute(
        builder: (BuildContext ctx) => const NewItem(),
      ),
    );
    if (newItem == null) {
      return;
    }
    //belwo setState method add the new entered items to the _groceryItems list
    setState(() {
      _groceryItems.add(newItem);
    });
  }
  */
//---------------------------------------------------------------
//BELOW CODE IS TO FETCH DATA FROM FIREBASE DATABASE
  void _addItem() async {
    final newItem = await Navigator.of(context).push<GroceryItem>(
      MaterialPageRoute(
        builder: (BuildContext ctx) => const NewItem(),
      ),
    );
    if (newItem == null) {
      return;
    }
    setState(() {
      _groceryItems.add(newItem);
    });
  }

  //  below method used to remove the grocery list items using dismissible widget
  void _removeItem(GroceryItem item) async {
    final index = _groceryItems.indexOf(item);
    setState(() {
      _groceryItems.remove(item);
    });
    final url = Uri.https(
      'udemy-flutter-99aea-default-rtdb.firebaseio.com',
      '/shopping-list/${item.id}.json',
    );
    final response = await http.delete(url);
    if (response.statusCode >= 400) {
      setState(() {
        _groceryItems.insert(index, item);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // below content widget show the messege when grocery list is empty
    Widget content = const Center(
      child: Text('No items added yet'),
    );
    //below if condition is used to add the loading
    if (_isLoading) {
      content = const Center(
        child: CircularProgressIndicator(),
      );
    }
    //below condition check the grocery list is empty or not if not then it show the  listview of that added grocery itemes
    if (_groceryItems.isNotEmpty) {
      content = ListView.builder(
        itemCount: _groceryItems.length,
        itemBuilder: ((ctx, index) =>
            //the ListTile widget is wrapped with Dismissible so user can remove/delete the data
            Dismissible(
              onDismissed: (direction) {
                _removeItem(_groceryItems[index]);
              },
              key: ValueKey(_groceryItems[index].id),
              child: ListTile(
                title: Text(_groceryItems[index].name,
                    style: const TextStyle(
                      fontSize: 22,
                    )),
                leading: Container(
                  width: 26,
                  height: 26,
                  color: _groceryItems[index].category.color,
                ),
                trailing: Text(_groceryItems[index].quantity.toString(),
                    style: const TextStyle(
                      fontSize: 22,
                    )), //trailing used to show the value of category in front of categories example 1st catergory item is milk so in front of milk we can show the properties of that categories like name , quantity or id
              ),
            )),
      );
    }
    if (_error != null) {
      content = Center(
        child: Text(_error!),
      );
    }
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 0, 57, 67),
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 255, 191, 0),
        title: const Text(
          'Your Groceries',
          style: TextStyle(
              fontSize: 28,
              color: Color.fromARGB(255, 0, 66, 97),
              fontWeight: FontWeight.bold),
        ),
        // below action widget from AppBar add the new IconButton on the right corner of the app bar (+) so when we press it navigate the screen to next page an in that page user can add the new items in a grocery list
        actions: [
          IconButton(
            onPressed: _addItem,
            icon: const Icon(Icons.add),
            color: const Color.fromARGB(255, 0, 60, 78),
            iconSize: 30,
            highlightColor: const Color.fromARGB(255, 0, 75, 94),
            hoverColor: Colors.redAccent,
          ),
        ],
      ),
      body:
          content, //passes that content in if condition to body of Scaffold to show on the main
    );
  }
}
